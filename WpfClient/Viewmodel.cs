﻿namespace WpfClient;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using CmtpLib;
using CmtpLib.Connection;

public class Viewmodel : INotifyPropertyChanged
{
    private readonly Connector connector;


    public event Action<(string Title, string Message)>? Alert;

    public Viewmodel(Connector connector)
    {
        this.connector = connector;
        this.connector.Alert += msg => Alert?.Invoke(("Error in library", msg));
        Name = connector.Name;
        ConnectCommand = new RelayCommand(_ => DoConnect(), _ => ipRegex.IsMatch(connectToIpText));
        this.connector.NeueNetzteilnehmer += peers =>
        {
            Peers = peers;
            OnPropertyChanged(nameof(Peers));
            if (Peers.Count == 0)
            {
                CurrentChatPartner = null;
            }
            else if(CurrentChatPartner == null)
            {
                CurrentChatPartner = Peers.First();
            }
        };
        var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
        connector.ReceiveMessage += (_, messageArgs) => Task.Factory.StartNew(() => AddMsg(messageArgs), CancellationToken.None, TaskCreationOptions.None, uiScheduler);
        connector.Empfangen += msg =>
        {
            messages[new(msg.PortSender, msg.AdresseSender, "")]
                    .Last(m => m.IsRead == false && (Encoding.UTF8.GetByteCount(m.Content) == msg.PayLoadLength))
                    .IsRead =
                true;
            OnPropertyChanged(nameof(Messages));
        };
    }

    private void AddMsg(ReceiveMessageEventArgs args) => messages.GetOrCreate(args.Sender)
        .Add(new(
            DateTime.Now,
            args.Sender,
            new(connector.Port, connector.Address, Name),
            args.Message));

    public string Name { get; private set; }

    public ICommand ChangeNameCommand => new RelayCommand(_ => ChangeName());

    private void ChangeName()
    {
        var picker = new NamePicker(Name);
        picker.ShowDialog();
        Name = picker.PickedName;
        connector.Name = Name;
        OnPropertyChanged(nameof(Name));
    }

    #region [ New connection ]

    private readonly Regex ipRegex = new("^([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})$");

    public string ConnectToIp
    {
        get => connectToIpText;
        set
        {
            connectToIpText = value;
            var match = ipRegex.Match(value);
            if (match.Success
                && byte.TryParse(match.Groups[1].Value, out var b0)
                && byte.TryParse(match.Groups[2].Value, out var b1)
                && byte.TryParse(match.Groups[3].Value, out var b2)
                && byte.TryParse(match.Groups[4].Value, out var b3))
            {
                connectToIp = new(b0, b1, b2, b3);
            }
            OnPropertyChanged();
        }
    }
    private string connectToIpText = "127.0.0.1";
    private IpAddress connectToIp = new(127, 0, 0, 1);

    public ushort ConnectToPort
    {
        get => connectToPort;
        set
        {
            connectToPort = value;
            OnPropertyChanged();
        }
    }
    private ushort connectToPort;

    public ICommand ConnectCommand { get; }

    private async void DoConnect()
    {
        try
        {
            _ = await connector.TryConnect(connectToPort, connectToIp);
        }
        catch (Exception exception)
        {
            Alert?.Invoke(("Critical error", exception.Message));
        }
    }

    #endregion

    #region [ Connected ]

    public string Id => connector.Id;

    private Participant? currentChatPartner;

    public Participant? CurrentChatPartner
    {
        get => currentChatPartner;
        set
        {
            currentChatPartner = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(Messages));
        }
    }

    public IReadOnlySet<Participant> Peers { get; set; } = new HashSet<Participant>();

    public ObservableCollection<Message> Messages => currentChatPartner == null? new() : messages.GetOrCreate(currentChatPartner);

    private readonly Dictionary<Participant, ObservableCollection<Message>> messages = new();
    private string messageText = "";

    public ICommand SendMessageCommand => new RelayCommand(_ => SendMessage(), _ => CurrentChatPartner != null && !string.IsNullOrWhiteSpace(MessageText));

    private void SendMessage()
    {
        if (CurrentChatPartner == null)
        {
            Alert?.Invoke(("Error", "No chat partner"));
            return;
        }

        var msg = MessageText;
        MessageText = "";
        connector.SendMessage(msg, CurrentChatPartner);
        Messages.Add(new(DateTime.Now, new(connector.Port, connector.Address, Name), CurrentChatPartner, msg)
            { IsRead = false });
    }

    public string MessageText
    {
        get => messageText;
        set
        {
            messageText = value;
            OnPropertyChanged();
        }
    }

    #endregion


    #region [ INPC ]

    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    public event PropertyChangedEventHandler? PropertyChanged; 

    #endregion

    public void Dispose()
    {
        connector.DisposeAsync();
    }
}