﻿namespace WpfClient;

using System.ComponentModel;
using System.Windows;
using CmtpLib;
using CmtpLib.Connection;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow
{
    public MainWindow()
    {
        InitializeComponent();
        var adr = GetIpAddress();
        var name = GetName();
        var viewmodel = new Viewmodel(new Connector(adr, name));
        viewmodel.Alert += OnAlert;
        DataContext = viewmodel;
        this.Title = $"CMTP - {viewmodel.Id}, {name}";
    }

    private IpAddress? GetIpAddress()
    {
        var picker = new IpAddressPicker();
        picker.ShowDialog();
        return picker.PickedAddress;
    }

    private string GetName()
    {
        var picker = new NamePicker("CMTP.NET");
        picker.ShowDialog();
        return picker.PickedName;
    }

    private void OnAlert((string Title, string Message) alert)
    {
        MessageBox.Show(this, alert.Message, alert.Title);
    }

    protected override void OnClosing(CancelEventArgs e)
    {
        ((Viewmodel)this.DataContext).Dispose();
        base.OnClosing(e);
    }
}