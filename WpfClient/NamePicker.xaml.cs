﻿namespace WpfClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/// <summary>
/// Interaction logic for NamePicker.xaml
/// </summary>
public partial class NamePicker : Window
{
    public string PickedName { get; set; }
    
    public NamePicker(string defaultPickedName)
    {
        PickedName = defaultPickedName;
        InitializeComponent();
    }

    public void Pick(object sender, RoutedEventArgs routedEventArgs) => DialogResult = true;
}
