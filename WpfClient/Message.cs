﻿namespace WpfClient;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CmtpLib;

public record Message(DateTime Timestamp, Participant Sender, Participant Receiver, string Content) : INotifyPropertyChanged
{
    private bool? isRead;

    public bool? IsRead
    {
        get => isRead;
        set
        {
            if (isRead != value)
            {
                isRead = value;
                Notify(nameof(IsRead)); 
            }
        }
    }

    public override string ToString()
    {
        return $"[{Timestamp}] {Sender}: {Content}";
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected virtual void Notify([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}