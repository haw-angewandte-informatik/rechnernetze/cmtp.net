﻿namespace WpfClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CmtpLib.Connection;

/// <summary>
/// Interaction logic for IpAddressPicker.xaml
/// </summary>
public partial class IpAddressPicker : Window
{
    public IpAddress? PickedAddress { get; set; }
    public List<IpAddress> Addresses { get; set; }
    public IpAddressPicker()
    {
        this.DataContext = this;
#pragma warning disable CS0618 // Type or member is obsolete
        Addresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(adr => adr.AddressFamily == AddressFamily.InterNetwork).Select(adr => (IpAddress)adr).ToList();
#pragma warning restore CS0618 // Type or member is obsolete
        InitializeComponent();
    }
    
    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
    {
        var btn = (Button)sender;
        PickedAddress = (IpAddress)btn.DataContext;
        DialogResult = true;
    }
}
