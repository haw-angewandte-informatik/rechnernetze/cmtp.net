# CMTP.NET

.NET 6 Client for CMTP

The WPF client runs on Windows only.  
The console client runs on windows, linux and MacOS.

The excutables are all self-contained, meaning you don't have to install a runtime first to use them.

Just download the artifact for your OS, extract and run the contained executable.