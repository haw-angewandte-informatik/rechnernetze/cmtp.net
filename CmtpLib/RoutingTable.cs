﻿namespace CmtpLib;

using System.Collections.Generic;
using System.Linq;
using Connection;

internal class RoutingTable
{
    /// <summary>
    /// Key: Gateway, value: Peers through that gateway
    /// </summary>
    private readonly Dictionary<Participant, List<Participant>> table = new();
    private HashSet<Participant> allPeers = new();

    public event Action<IReadOnlySet<Participant>>? RoutingChanged;

    public void IntegrateBinDa(Participant gateway, List<Participant> entries)
    {
        // Alle Teilnehmer ignorieren, die wir schon kennen
        var otherPeers = GetAllExceptVia(gateway);
        entries.RemoveAll(p => otherPeers.Contains(p));
        if (!table.ContainsKey(gateway) || !table[gateway].SequenceEqual(entries, Participant.CompleteComparer))
        {
            // Alle indirekten Routen zum Gateway löschen
            foreach (var participants in table.Values)
            {
                participants.Remove(gateway);
            }
            // Neue Routen einfügen
            table[gateway] = entries;
            UpdateRouting();
        }

        // Update name in case the other one changed it
        if (table.Keys.Single(p => p == gateway).Name != gateway.Name)
        {
            table.Keys.Single(p => p == gateway).Name = gateway.Name;
            UpdateRouting();
        }
    }

    private void UpdateRouting()
    {
        // Sowohl Gateways als auch indirekte Teilnehmer können wir erreichen
        allPeers = table.Keys.ToHashSet();
        allPeers.UnionWith(table.Values.SelectMany(v => v));
        RoutingChanged?.Invoke(allPeers);
    }

    /// <summary>
    /// Returns the gateway for the given target. Throws if no gateway is found.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    /// <exception cref="KeyNotFoundException"></exception>
    public Participant GetGateway(Participant target) =>
        table.ContainsKey(target) ? target 
        : table.Keys.SingleOrDefault(k => table.TryGetValue(k, out var targets) && targets.Contains(target)) ??
        throw new KeyNotFoundException($"No route for target {target}");

    /// <summary>
    /// Remove gateway when we receive a "BinWeg"
    /// </summary>
    /// <param name="gateway"></param>
    public void RemoveGateway(Participant gateway)
    {
        table.Remove(gateway);
        UpdateRouting();
    }

    public List<Participant> GetGateways() => table.Keys.ToList();

    /// <summary>
    /// Return all peers except those reachable through the given gateway.
    /// Used for split horizon.
    /// </summary>
    /// <param name="gateway"></param>
    /// <returns></returns>
    public IEnumerable<Participant> GetAllExceptVia(Participant gateway)
    {
        return table.SelectMany(entry => 
            entry.Key == gateway 
                ? new List<Participant>(0) 
                : entry.Value.Concat(new List<Participant>{entry.Key}));
    }

    public void AddGateway(Participant participant)
    {
        IntegrateBinDa(participant, new());
    }

    public Participant GetPeer(IpAddress adr, ushort port)
    {
        return allPeers.First(p => p.Address == adr && p.Port == port);
    }
}