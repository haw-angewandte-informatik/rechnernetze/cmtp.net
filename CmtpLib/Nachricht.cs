﻿namespace CmtpLib;

using Connection;

public struct Nachricht
{
    public MessageHeader Header;
    public string Content;

    public static Nachricht From(Participant sender)
    {
        return new Nachricht
        {
            Header = new MessageHeader
            {
                PortSender = sender.Port,
                AdresseSender = sender.Address,
                NachrichtenTyp = MessageType.Nachricht
            }
        };
    }

    public Nachricht To(Participant receiver)
    {
        Header.PortEmpfaenger = receiver.Port;
        Header.AdresseEmpfaenger = receiver.Address;
        return this;
    }

    public Nachricht WithContent(string content)
    {
        Content = content;
        return this;
    }

    public Nachricht(MessageHeader header, string content)
    {
        Header = header;
        Content = content;
    }
}
