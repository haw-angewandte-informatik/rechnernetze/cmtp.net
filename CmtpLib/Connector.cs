﻿namespace CmtpLib;

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Connection;

/// <summary>
/// Stellt einem Client Funktionen und Events zur Interaktion mit CMTP zur Verfügung.
/// </summary>
public class Connector : IAsyncDisposable
{
    public string Id => $"{Address}:{Port}";
    public ushort Port 
    {
        get => hub.Port;
        set => hub.Port = value; 
    }

    public IpAddress Address 
    {
        get => hub.Adresse;
        set => hub.Adresse = value;
    }

    public string Name
    {
        get => hub.Name;
        set => hub.Name = value;
    }

    private readonly Hub hub;

    /// <summary>
    /// Nachricht eines anderen Teilnehmers erhalten.
    /// </summary>
    public event EventHandler<ReceiveMessageEventArgs>? ReceiveMessage;

    /// <summary>
    /// Feuert, wenn es neue Netzteilnehmer gibt. Payload enthält alle derzeitigen Netzteilnehmer.
    /// </summary>
    public event Action<IReadOnlySet<Participant>>? NeueNetzteilnehmer;

    public event Action<MessageHeader>? Empfangen;

    public event Action<string>? Alert;

    public Connector(IpAddress? adr = null, string name = "CMTP.NET")
    {
        hub = new(adr);
        hub.Alert += msg => Alert?.Invoke(msg);
        hub.Nachricht += msg => ReceiveMessage?.Invoke(this,
            msg);
        hub.NeueNetzteilnehmer += peers => NeueNetzteilnehmer?.Invoke(peers);
        hub.Empfangen += msg => Empfangen?.Invoke(msg);
        Name = name;
    }

    /// <summary>
    /// Verbindungsaufbau
    /// </summary>
    /// <param name="port">What port to connect to.</param>
    /// <param name="address">IP Address of the participant to connect to.</param>
    /// <returns>True if the connection was successful, otherwise false.</returns>
    public async Task<bool> TryConnect(ushort port, IpAddress address)
    {
        return await hub.Connect(port, address);
    }

    /// <summary>
    /// Nachrichtenübertragung
    /// </summary>
    /// <param name="message"></param>
    /// <param name="receiver"></param>
    public void SendMessage(string message, Participant receiver)
    {
        hub.HandleNachricht(Nachricht.From(new(Port, Address, Name)).To(receiver).WithContent(message));
    }
    
    public ValueTask DisposeAsync()
    {
        return hub.DisposeAsync();
    }
}
