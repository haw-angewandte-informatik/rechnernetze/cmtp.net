﻿namespace CmtpLib;

using System;

public class ReceiveMessageEventArgs : EventArgs
{
    public Participant Sender;
    public string Message;

    public ReceiveMessageEventArgs(Participant sender, string message)
    {
        Sender = sender;
        Message = message;
    }
}
