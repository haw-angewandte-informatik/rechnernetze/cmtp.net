﻿namespace CmtpLib.Connection;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using CmtpLib;

internal class SocketAdapter : IAsyncDisposable
{
    private static readonly ushort[] DefaultPorts = { 80, 40000, 50000, 60000, 0 };
    
    private readonly Socket socket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

    public event Action<(MessageHeader Header, List<Participant>)>? BinDa;
    public event Action<Nachricht>? Nachricht;
    public event Action<MessageHeader>? Empfangen;
    public event Action<MessageHeader>? BinWeg;

    // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
    private readonly Task listeningLoop;

    private readonly (ushort Port, IpAddress Address) localId;
    public (ushort, IpAddress) GetLocalId() => localId;
    public string Name { get; set; } = "";

    private static void BindToNicePort(Socket s, IpAddress adr)
    {
        foreach (var port in DefaultPorts)
        {
            try
            {
                s.Bind(new IPEndPoint(IPAddress.Parse(adr.AsDottedDecimal), port));
                break;
            }
            catch (SocketException)
            {
                Debug.WriteLine($"{port} not available");
            }
        }
    }
    private static IPAddress GetDefaultAddress()
    {
        return Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(a =>
                   a.AddressFamily == AddressFamily.InterNetwork && a.GetAddressBytes()[0] == 192) ??
               Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(a =>
                   a.AddressFamily == AddressFamily.InterNetwork && a.GetAddressBytes()[0] == 141) ??
               Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(a =>
                   a.AddressFamily == AddressFamily.InterNetwork);
    }
    
    public SocketAdapter(IpAddress? ipToUse = null)
    {
        var adr = ipToUse ?? GetDefaultAddress();
        BindToNicePort(socket, adr);
        localId = ((ushort)socket.LocalEndPoint!.As<IPEndPoint>()!.Port, adr);
        socket.Listen();
        listeningLoop = Task.Run(async () => await ListenToIncoming(CancellationToken.None));
        listeningLoop.ConfigureAwait(false);
    }

    private readonly Dictionary<Participant, (CancellationTokenSource Cts, Task Connection)> connections = new();
    private readonly Dictionary<Participant, NetworkStream> outputStreams = new();

    private async Task ListenToIncoming(CancellationToken token)
    {
        await Task.Yield();
        Debug.WriteLine("Listening");
        while (!token.IsCancellationRequested)
        {
            var newConnection = await socket.AcceptAsync(token);
            var endpoint = (IPEndPoint)newConnection.RemoteEndPoint!;
            Debug.WriteLine($"Connection accepted from {endpoint.Address}");

            var cts = new CancellationTokenSource();
            var stream = new NetworkStream(newConnection);
            var readBinDaTask = ReadHeader(stream, token).ContinueWith(async t =>
            {
                var header = await t;
                var binDa = await MessageIO.ParseBinDa(header, stream);
                return binDa;
            }, token);
            
            var newGateway = (await await readBinDaTask).First();

            if (connections.TryGetValue(newGateway, out var curr))
            {
                curr.Cts.Cancel();
            }

            var newTask = RunGateway(newGateway, stream, cts.Token);
#pragma warning disable CS4014
            newTask.ConfigureAwait(false);
#pragma warning restore CS4014
            connections[newGateway] = (cts, newTask);

            BinDa?.Invoke((new(), new[] { newGateway }.ToList()));
        }
    }

    private async Task SetupConnection(Socket newConnection, Participant gateway)
    {
        var cts = new CancellationTokenSource();
        var stream = new NetworkStream(newConnection);
        await SendBinDaInternal(
            stream,
            new List<Participant> { new(localId.Port, localId.Address, Name) },
            gateway);

        var newTask = RunGateway(gateway, stream, cts.Token);
#pragma warning disable CS4014
        newTask.ConfigureAwait(false);
#pragma warning restore CS4014
        connections[gateway] = (cts, newTask);
    }

    /// <summary>
    /// Keeps a connection open.
    /// </summary>
    /// <param name="gateway"></param>
    /// <param name="stream"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    private async Task RunGateway(Participant gateway, NetworkStream stream, CancellationToken token)
    {
        // Stream zum scheiben in eine Liste speichern
        outputStreams[gateway] = stream;
        Debug.WriteLine($"Stream zum Gateway {gateway.Address}:{gateway.Port} geöffnet");
        try
        {
            while (!token.IsCancellationRequested)
            {
                while (!stream.DataAvailable)
                {
                    await Task.Delay(500, token);
                }

                // Jede Nachricht einfach deserialisieren
                var header = await ReadHeader(stream, token);
                await HandleMessage(header, stream).ConfigureAwait(false);
            }
        }
        catch (Exception exc)
        {
            Debug.WriteLine($"Exception in RunGateway: {exc}");
            BinWeg?.Invoke(new()
            {
                AdresseSender = gateway.Address,
                PortSender = gateway.Port,
                AdresseEmpfaenger = localId.Address,
                PortEmpfaenger = localId.Port,
                NachrichtenTyp = MessageType.BinWeg
            });
        }

        outputStreams.Remove(gateway);
        WriteHeader(stream, new()
        {
            AdresseSender = localId.Address, 
            PortSender = localId.Port, 
            AdresseEmpfaenger = gateway.Address, 
            PortEmpfaenger = gateway.Port, 
            NachrichtenTyp = MessageType.BinWeg
        });
        await stream.DisposeAsync();
        Debug.WriteLine($"Verbindung zum Gateway {gateway.Address}:{gateway.Port} geschlossen");
    }

    /// <summary>
    /// Reads exactly one header (20 byte) from a stream.
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    private async Task<MessageHeader> ReadHeader(Stream stream, CancellationToken token)
    {
        MessageHeader header = new();
        header.AdresseEmpfaenger = await stream.ReadAddress(token);
        header.PortEmpfaenger = stream.ReadUShort();
        header.PortSender = stream.ReadUShort();
        header.AdresseSender = stream.ReadAddress();
        header.NachrichtenTyp = (MessageType)stream.ReadUShort();
        header.Checksumme = stream.ReadUShort();
        header.PayLoadLength = stream.ReadUInt();
        Debug.WriteLine($"REC: {header}");
        return header;
    }

    private void WriteHeader(Stream stream, MessageHeader header)
    {
        Debug.WriteLine($"Sending HEADER {header}");
        stream.WriteAddress(header.AdresseEmpfaenger);        // 4 byte
        stream.WriteUShort(header.PortEmpfaenger);            // 2 byte
        stream.WriteUShort(header.PortSender);                // 2 byte
        stream.WriteAddress(header.AdresseSender);            // 4 byte
        stream.WriteUShort((ushort)header.NachrichtenTyp);    // 2 byte
        stream.WriteUShort(header.Checksumme);                // 2 byte
        stream.WriteUint(header.PayLoadLength);               // 4 byte
    }

    /// <summary>
    /// Deserializes a message
    /// </summary>
    /// <param name="header"></param>
    /// <param name="stream"></param>
    private async Task HandleMessage(MessageHeader header, Stream stream)
    {
        switch (header.NachrichtenTyp)
        {
            case MessageType.BinDa:
                var binDa = await MessageIO.ParseBinDa(header, stream);
                Debug.WriteLine($"{binDa[0]}: BinDa ({binDa.Count})");
                lock (binDaLock)
                {
                    BinDa?.Invoke((header, binDa)); 
                }
                break;
            case MessageType.Nachricht:
                var nachricht = await MessageIO.ParseNachricht(header, stream);
                lock (nachrichtLock)
                {
                    Nachricht?.Invoke(new(header, nachricht)); 
                }
                break;
            case MessageType.Empfangen:
                lock (empfangenLock)
                {
                    Empfangen?.Invoke(header); 
                }
                break;
            case MessageType.BinWeg:
                lock (binWegLock)
                {
                    BinWeg?.Invoke(header); 
                }
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(header.NachrichtenTyp), header.NachrichtenTyp, "Ungültiger Nachrichtentyp");
        }
    }

    private readonly object binDaLock = new();
    private readonly object nachrichtLock = new();
    private readonly object empfangenLock = new();
    private readonly object binWegLock = new();

    /// <summary>
    /// Sends the header to the gateway. Does not manipulate anything on the way.
    /// </summary>
    /// <param name="header"></param>
    /// <param name="gateway"></param>
    public void SendEmpfangen(MessageHeader header, Participant gateway)
    {
        var stream = outputStreams[gateway];

        WriteHeader(stream, header);
    }

    public async Task SendNachricht(Nachricht msg, Participant gateway)
    {
        var stream = outputStreams[gateway];

        msg.Header.PayLoadLength = (uint)Encoding.UTF8.GetByteCount(msg.Content);
        WriteHeader(stream, msg.Header);

        await MessageIO.WriteNachricht(stream, msg.Content);
    }

    public async Task SendBinDa(List<Participant> participants, Participant gateway)
    {
        var stream = outputStreams[gateway];
        
        var header = new MessageHeader
        {
            AdresseSender = localId.Address,
            PortSender = localId.Port,
            AdresseEmpfaenger = gateway.Address,
            PortEmpfaenger = gateway.Port,
            Checksumme = 0,
            NachrichtenTyp = MessageType.BinDa,
            PayLoadLength = (uint)participants.Count
        };
        
        WriteHeader(stream, header);
        await MessageIO.WriteBinDa(stream, participants);
    }

    private async Task SendBinDaInternal(Stream stream, List<Participant> participants, Participant gateway)
    {
        var header = new MessageHeader
        {
            AdresseSender = localId.Address,
            PortSender = localId.Port,
            AdresseEmpfaenger = gateway.Address,
            PortEmpfaenger = gateway.Port,
            Checksumme = 0,
            NachrichtenTyp = MessageType.BinDa,
            PayLoadLength = (uint)participants.Count
        };

        WriteHeader(stream, header);
        await MessageIO.WriteBinDa(stream, participants);
    }

    public async Task Connect(ushort port, IpAddress address, CancellationToken token)
    {
        var oldParticipant = new Participant(port, address, "");
        if (connections.TryGetValue(oldParticipant, out var curr))
        {
            curr.Cts.Cancel();
        }

        var newConnection = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        newConnection.Bind(new IPEndPoint(IPAddress.Parse(localId.Address.AsDottedDecimal), 0));
        await newConnection.ConnectAsync(
            IPAddress.Parse(address.AsDottedDecimal), port, token);
        Debug.WriteLine($"Connected to {address}:{port}");

        _ = Task.Run(() => SetupConnection(newConnection, oldParticipant), token);
    }

    public async ValueTask DisposeAsync()
    {
        await Task.WhenAll(connections.Values.Select(x =>
        {
            x.Cts.Cancel();
            return x.Connection;
        }));
    }
}
