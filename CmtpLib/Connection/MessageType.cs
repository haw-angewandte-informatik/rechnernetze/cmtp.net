﻿namespace CmtpLib.Connection;

public enum MessageType : ushort
{
    BinDa = 1,
    BinWeg = 2,
    Nachricht = 3,
    Empfangen = 4
}
