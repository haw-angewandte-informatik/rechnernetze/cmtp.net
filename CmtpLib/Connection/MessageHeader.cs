﻿namespace CmtpLib.Connection;

using System.Net;

public struct MessageHeader
{
    public IpAddress AdresseEmpfaenger;
    public ushort PortEmpfaenger, PortSender;
    public IpAddress AdresseSender;
    public MessageType NachrichtenTyp;
    public ushort Checksumme;
    public uint PayLoadLength;

    public override string ToString() => $"{NachrichtenTyp} {AdresseSender}:{PortSender}->{AdresseEmpfaenger}:{PortEmpfaenger} ({PayLoadLength})";
}

/// <summary>
/// 
/// </summary>
/// <param name="Octett1">Leftmost octet</param>
/// <param name="Octett2"></param>
/// <param name="Octett3"></param>
/// <param name="Octett4">Rightmost octet</param>
public record struct IpAddress(byte Octett1, byte Octett2, byte Octett3, byte Octett4)
{
    /// <summary>
    /// Returns the value of this IPv4 address in dotted-decimal format.
    /// </summary>
    public override string ToString() => $"{Octett1}.{Octett2}.{Octett3}.{Octett4}";

    public string AsDottedDecimal => ToString();

    public byte[] AsBigEndian => new byte[] { Octett1, Octett2, Octett3, Octett4 };

    public static IpAddress FromBigEndian(Span<byte> bytes)
    {
        return bytes.Length != 4
            ? throw new ArgumentException("IPv4 address must be 4 bytes long")
            : new IpAddress(bytes[0], bytes[1], bytes[2], bytes[3]);
    }

    public static implicit operator IpAddress(uint adr)
    {
        var bytes = BitConverter.GetBytes(adr);
        return new(bytes[3], bytes[2], bytes[1], bytes[0]);
    }

    public static implicit operator uint(IpAddress adr)
    {
        return BitConverter.ToUInt32(new byte[]
        {
            adr.Octett4,
            adr.Octett3,
            adr.Octett2,
            adr.Octett1,
        });
    }

    public static implicit operator IpAddress(IPAddress systemAdr)
    {
        byte[] bytes = systemAdr.GetAddressBytes();
        return new(bytes[0], bytes[1], bytes[2], bytes[3]);
    }
}