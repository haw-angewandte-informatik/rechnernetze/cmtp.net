﻿namespace CmtpLib;
using System.Text;

using CmtpLib.Connection;

internal static class MessageIO
{
    private static List<byte> ReadToNull(this Stream stream, bool includeNull = false)
    {
        int readByte;

        var bytes = new List<byte>();

        while ((readByte=stream.ReadByte()) != 0x00)
        {
            bytes.Add((byte)readByte);
        }

        if(includeNull)
            bytes.Add(0x00);

        return bytes;
    }

    public static async Task<List<Participant>> ParseBinDa(MessageHeader header, Stream stream)
    {
        List<Participant> parts = new List<Participant>((int)header.PayLoadLength);

        for (int i = 0; i < header.PayLoadLength; i++)
        {
            var ip = new IpAddress(
                (byte)stream.ReadByte(),
                (byte)stream.ReadByte(),
                (byte)stream.ReadByte(),
                (byte)stream.ReadByte());

            var port = stream.ReadUShort();

            var name = Encoding.UTF8.GetString(stream.ReadToNull().ToArray());


            parts.Add(
                new Participant(
                    port,
                    ip,
                    name
                ));
        }

        return parts;
    }

    public static Task WriteBinDa(Stream stream, IEnumerable<Participant> participants)
    {
        foreach (var participant in participants)
        {
            IpAddress ip = participant.Address;

            ushort port = participant.Port;

            byte[] nameBytes = Encoding.UTF8.GetBytes(participant.Name);

            stream.WriteByte(ip.Octett1);
            stream.WriteByte(ip.Octett2);
            stream.WriteByte(ip.Octett3);
            stream.WriteByte(ip.Octett4);

            stream.WriteUShort(port);

            stream.Write(nameBytes);
            stream.WriteByte(0x00);
        }

        return Task.CompletedTask;
    }

    public static async Task<string> ParseNachricht(MessageHeader header, Stream stream)
    {
        byte[] bytes = new byte[header.PayLoadLength];

        _ = await stream.ReadAsync(bytes);

        return Encoding.UTF8.GetString(bytes);
    }

    public static async Task WriteNachricht(Stream stream, string content)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(content);

        await stream.WriteAsync(bytes);
    }
}