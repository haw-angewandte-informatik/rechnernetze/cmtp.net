﻿namespace CmtpLib;

using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Connection;

public static class Extensions
{
    public static ushort ReadUShort(this Stream stream)
    {
        Span<byte> bytes = stackalloc byte[2];

        _ = stream.Read(bytes);
        
        return BitConverter.ToUInt16(bytes).EnsureEndianness();
    }

    public static uint ReadUInt(this Stream stream)
    {
        Span<byte> bytes = stackalloc byte[4];
        
        _ = stream.Read(bytes);

        return BitConverter.ToUInt32(bytes).EnsureEndianness();
    }

    public static async Task<uint> ReadUInt(this Stream stream, CancellationToken token)
    {
        var bytes = new Memory<byte>(new byte[4]);

        _ = await stream.ReadAsync(bytes, token);
        
        return BitConverter.ToUInt32(bytes.Span).EnsureEndianness();
    }

    public static void WriteAddress(this Stream stream, IpAddress address)
    {
        stream.Write(address.AsBigEndian);
    }

    public static IpAddress ReadAddress(this Stream stream)
    {
        Span<byte> bytes = stackalloc byte[4];

        _ = stream.Read(bytes);

        return IpAddress.FromBigEndian(bytes);
    }

    public static async Task<IpAddress> ReadAddress(this Stream stream, CancellationToken token)
    {
        var bytes = new Memory<byte>(new byte[4]);

        _ = await stream.ReadAsync(bytes, token);

        return IpAddress.FromBigEndian(bytes.Span);
    }

    public static void WriteUShort(this Stream stream, ushort value)
    {
        var bytes = BitConverter.GetBytes(value.EnsureEndianness());

        stream.Write(bytes);
    }

    public static void WriteUint(this Stream stream, uint value)
    {
        byte[] bytes = BitConverter.GetBytes(value.EnsureEndianness());

        stream.Write(bytes);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static TValue GetOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key)
        where TValue : new()
        where TKey : notnull
    {
        if (self.TryGetValue(key, out TValue? value))
        {
            return value;
        }

        TValue newVal = new();

        self[key] = newVal;

        return newVal;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static TValue GetOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key, Func<TValue> creator)
        where TKey : notnull
    {
        if (self.TryGetValue(key, out TValue? value))
        {
            return value;
        }

        TValue newVal = creator();

        self[key] = newVal;

        return newVal;
    }

    public static T? As<T>(this object obj)
        where T : class
    {
        return obj as T;
    }

    public static ushort EnsureEndianness(this ushort input)
    {
        return BitConverter.IsLittleEndian ? BinaryPrimitives.ReverseEndianness(input) : input;
    }

    public static uint EnsureEndianness(this uint input)
    {
        return BitConverter.IsLittleEndian ? BinaryPrimitives.ReverseEndianness(input) : input;
    }
}