﻿namespace CmtpLib;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Connection;

/// <summary>
/// Zuständig für das Filtern von Nachrichten und verarbeiten von solchen, die bloß weitergeleitet werden sollen.
/// </summary>
internal class Hub : IAsyncDisposable
{
    /// <summary>
    /// Eigener Port.
    /// </summary>
    public ushort Port { get; set; }

    /// <summary>
    /// Eigene Adresse.
    /// </summary>
    public IpAddress Adresse { get; set; }

    public string Name
    {
        get => adapter.Name;
        set => adapter.Name = value;
    }

    /// <summary>
    /// Feuert, wenn eine an uns adressierte Nachricht vom Typ <see cref="MessageType.Nachricht"/> empfangen wurde.
    /// </summary>
    public event Action<ReceiveMessageEventArgs>? Nachricht;

    /// <summary>
    /// Feuert, wenn eine an uns adressierte Nachricht vom Typ <see cref="MessageType.Empfangen"/> empfangen wurde.
    /// </summary>
    public event Action<MessageHeader>? Empfangen;

    public event Action<IReadOnlySet<Participant>>? NeueNetzteilnehmer;

    public event Action<string>? Alert;

    private readonly SocketAdapter adapter;
    private readonly RoutingTable routingTable = new();
    private (CancellationTokenSource Token, Task Looper) binDaLooper;

    public Hub(IpAddress? ipToUse = null)
    {
        routingTable.RoutingChanged += peers => NeueNetzteilnehmer?.Invoke(peers);

        adapter = new(ipToUse);
        adapter.BinDa += HandleBinDa;
        adapter.Nachricht += HandleNachricht;
        adapter.Empfangen += HandleEmpfangen;
        adapter.BinWeg += HandleBinWeg;
        (Port, Adresse) = adapter.GetLocalId();
        CancellationTokenSource cts = new();

        async Task Loop(CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1), token);
                    var gateways = routingTable.GetGateways();
                    Debug.WriteLine($"Sending BinDa to {gateways.Count} connected gateways");
                    await Task.WhenAll(gateways.Select(g =>
                    {
                        var allExceptVia = routingTable.GetAllExceptVia(g).ToList();
                        //Debug.WriteLine($"Sending BinDa with {allExceptVia.Count} peers to {g}");
                        return adapter.SendBinDa(
                                new List<Participant> { GetMe() }
                                    .Concat(allExceptVia).ToList(), g);
                    }));
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine($"Exception in binDaLooper: {exc}");
                binDaLooper.Token.Cancel();
                var nextCts = new CancellationTokenSource();
                binDaLooper = (nextCts, Task.Run(async () => await Loop(nextCts.Token), nextCts.Token));
            }
        }

        binDaLooper = (cts, Task.Run(async () => await Loop(cts.Token), cts.Token));
    }

    private bool IstFuerUns(MessageHeader header) => header.AdresseEmpfaenger == Adresse && header.PortEmpfaenger == Port;

    private void HandleBinWeg(MessageHeader msg)
    {
        // Absender aus Routingtabelle entfernen
        routingTable.RemoveGateway(new(msg.PortSender, msg.AdresseSender, ""));
    }

    private void HandleEmpfangen(MessageHeader msg)
    {
        if (IstFuerUns(msg))
        {
            Debug.WriteLine($"{routingTable.GetPeer(msg.AdresseSender, msg.PortSender)}: Empfangen {msg.PayLoadLength}");
            Empfangen?.Invoke(msg);
        }
        else
        {
            // Routing
            var gateway = routingTable.GetGateway(new(msg.PortEmpfaenger, msg.AdresseEmpfaenger, ""));
            adapter.SendEmpfangen(msg, gateway);
        }
    }

    internal async void HandleNachricht(Nachricht msg)
    {
        if (IstFuerUns(msg.Header))
        {
            Nachricht?.Invoke(new(routingTable.GetPeer(msg.Header.AdresseSender, msg.Header.PortSender), msg.Content));
            var empfangen = new MessageHeader
            {
                AdresseSender = Adresse,
                PortSender = Port,
                AdresseEmpfaenger = msg.Header.AdresseSender,
                PortEmpfaenger = msg.Header.PortSender,
                NachrichtenTyp = MessageType.Empfangen,
                PayLoadLength = (uint)Encoding.UTF8.GetByteCount(msg.Content)
            };
            adapter.SendEmpfangen(empfangen, routingTable.GetGateway(new(msg.Header.PortSender, msg.Header.AdresseSender, "")));
        }
        else
        {
            // Routing
            var gateway = routingTable.GetGateway(new(msg.Header.PortEmpfaenger, msg.Header.AdresseEmpfaenger, ""));
            await adapter.SendNachricht(msg, gateway);
        }
    }

    private void HandleBinDa((MessageHeader Header, List<Participant> peerlist) teilnehmer)
    {
        // In eigene Routingtabelle integrieren
        routingTable.IntegrateBinDa(teilnehmer.peerlist[0], teilnehmer.peerlist.Skip(1).ToList());
    }

    private Participant GetMe() => new(Port, Adresse, Name);

    /// <summary>
    /// Stellt die Verbindung mit einem anderen Teilnehmer her.
    /// </summary>
    /// <param name="port">Port des anderen Teilnehmers.</param>
    /// <param name="address">Adresse des anderen Teilnehmers.</param>
    /// <param name="message">Konnte die Verbindung nicht hergestellt werden, wird hier eine Fehlermeldung geliefert.</param>
    /// <returns>True falls die Verbindung erfolgreich hergestellt wurde, sonst false.</returns>
    public async Task<bool> Connect(ushort port, IpAddress address)
    {
        // Verbindung zum angegebenen Teilnehmer herstellen
        try
        {
            await adapter.Connect(port, address, CancellationToken.None);
        }
        catch (Exception e)
        {
            Alert?.Invoke($"Verbindung zu {address}:{port} fehlgeschlagen: {e.Message}");
            return false;
        }
        
        return true;
    }
    
    public async ValueTask DisposeAsync()
    {
        binDaLooper.Token.Cancel();
        await binDaLooper.Looper;
        await adapter.DisposeAsync();
    }
}
