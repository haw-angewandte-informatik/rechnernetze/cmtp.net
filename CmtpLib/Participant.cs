﻿namespace CmtpLib;

using Connection;

public record Participant(ushort Port, IpAddress Address, string Name)
{
    internal static IEqualityComparer<Participant> CompleteComparer => new CompleteCopmarer();
    
    public string ToId() => $"{Address}:{Port}";

    public override string ToString() => $"{Name}";

    /// <summary>
    /// Compares equality of the ID.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public virtual bool Equals(Participant? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Port == other.Port && Address.Equals(other.Address);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Port, Address);
    }

    public string Name { get; set; } = Name;

    internal class CompleteCopmarer : IEqualityComparer<Participant>
    {
        public bool Equals(Participant? x, Participant? y)
        {
            return ReferenceEquals(x, y) || 
                   (x!.Name == y!.Name && x.Port == y.Port && x.Address.Equals(y.Address));
        }

        public int GetHashCode(Participant obj)
        {
            unchecked
            {
                var hashCode = obj.Name.GetHashCode();
                hashCode = (hashCode * 397) ^ obj.Port.GetHashCode();
                hashCode = (hashCode * 397) ^ obj.Address.GetHashCode();
                return hashCode;
            }
        }
    }
}
