﻿using CmtpLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient_Console
{
    internal class ChatRoom
    {
        const int VERTICAL_SPACING = 2;
        public Participant Participant { get; }
        public string Name => Participant.Name;

        List<(bool isMine, string message)> chatHistory = new List<(bool isMine, string message)> ();


        const string paddingStr = "   ";
        string messageString = "";
        const int MAX_LINE_WIDTH = 50;

        bool isInConsoleLoop = false;

        int topMost = 1+VERTICAL_SPACING;
        readonly List<string> lines = new List<string>() { "" };


        const string enterTextString = "Enter Text:\n" + paddingStr;

        public ChatRoom(Participant participant)
        {
            Participant = participant;
        }

        public void RecieveMessage(string message, object consoleLock)
        {
            chatHistory.Add((false, message));

            if (isInConsoleLoop)
            {
                lock (consoleLock)
                {
                    AddMessage(message, 50, ConsoleColor.Green);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.CursorLeft--;
                }
            }
        }

        public void AddMessage(string message, int offsetX, ConsoleColor color)
        {
            //clear screen below last message
            string clearString = "";
            for (int i = 0; i < Console.CursorTop - topMost + 1; i++)
            {
                clearString += new string(' ', Console.BufferWidth);
            }

            Console.SetCursorPosition(0, topMost);

            Console.Write(clearString);

            Console.SetCursorPosition(0, topMost);

            Console.ForegroundColor = color;
            topMost += ChatUtils.PrettyPrintMessage(message, offsetX, MAX_LINE_WIDTH) + VERTICAL_SPACING;
            Console.ForegroundColor = ConsoleColor.White;

            Console.SetCursorPosition(0, Console.CursorTop+VERTICAL_SPACING);

            Console.Write(enterTextString);


            for (int i = 0; i < lines.Count; i++)
            {
                if (i > 0)
                    Console.Write(paddingStr);

                string? line = lines[i];
                if (line.EndsWith("\n"))
                {
                    Console.Write(line[0..^1]);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("\\n");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                    Console.Write(line);

                if (i < lines.Count - 1)
                    Console.WriteLine();
            }
        }


        public void ConsoleLoop(object consoleLock, Connector connector)
        {
            isInConsoleLoop = true;

            lock (consoleLock)
            {
                Console.Clear();
                Console.SetCursorPosition(0, 0);

                Console.WriteLine("ChatRoom: " + Name);
                Console.CursorTop += VERTICAL_SPACING;
                foreach (var (isMine, message) in chatHistory)
                {
                    if (isMine)
                    {
                        topMost = ChatUtils.PrettyPrintMessage(message + "\n\n", 2, MAX_LINE_WIDTH)+VERTICAL_SPACING;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        topMost = ChatUtils.PrettyPrintMessage(message + "\n\n", 50, MAX_LINE_WIDTH)+VERTICAL_SPACING;
                    }

                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.Write(enterTextString);

                Console.CursorVisible = false;

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|");
                Console.ForegroundColor = ConsoleColor.White;
                Console.CursorLeft--;
            }


            while (true)
            {
                //¦⁞
                var left = Console.CursorLeft;

                var info = new ConsoleKeyInfo();

                info = Console.ReadKey(true);

                lock (consoleLock)
                {
                    if (info.Key == ConsoleKey.Enter)
                    {
                        if (info.Modifiers == 0 && messageString.Length>0)
                        {
                            lines.Clear();
                            lines.Add("");
                            AddMessage(messageString, 2, ConsoleColor.White);

                            connector.SendMessage(messageString, Participant);

                            chatHistory.Add((true, messageString));
                            messageString = "";
                        }
                        else if (info.Modifiers == ConsoleModifiers.Shift)
                        {
                            Console.CursorLeft = left;
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write($"\\n\n{paddingStr}");
                            lines[^1] += '\n';
                            Console.ForegroundColor = ConsoleColor.White;
                            lines.Add("");
                            messageString += '\n';
                        }
                        else
                        {
                            Console.CursorLeft = left;
                        }
                    }
                    else if (info.Key == ConsoleKey.Backspace)
                    {
                        if (Console.CursorLeft < paddingStr.Length && lines.Count > 1)
                        {
                            Console.Write("  ");

                            lines.RemoveAt(lines.Count - 1);
                            Console.SetCursorPosition(paddingStr.Length + lines[^1].Length - 1,
                                Console.CursorTop - 1);

                            if (messageString[^1] == '\n')
                            {
                                Console.Write("  \b\b");
                            }
                        }
                        else if (messageString.Length > 0)
                        {
                            Console.Write("\b  \b\b");
                        }


                        
                        if (messageString.Length > 0)
                        {
                            lines[^1] = lines[^1][0..^1];
                            messageString = messageString[0..^1];
                        }
                    }
                    else if(info.Key == ConsoleKey.Escape)
                    {
                        isInConsoleLoop = false;
                        return;
                    }
                    else
                    {
                        Console.Write(info.KeyChar);

                        lines[^1] += info.KeyChar;
                        messageString += info.KeyChar;
                    }

                    if (Console.CursorLeft - paddingStr.Length == MAX_LINE_WIDTH)
                    {
                        Console.CursorLeft = paddingStr.Length;
                        Console.CursorTop++;
                        lines.Add("");
                    }

                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.CursorLeft--;
                }


            }
        }
    }
}
