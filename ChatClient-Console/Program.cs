﻿using ChatClient_Console;
using CmtpLib;
using CmtpLib.Connection;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

//PrintMessage(
//    "Test for checking if this actually works, causeithinkthatifIusealongwordeverythigwillbreak but maybe not... also I would love to know if newlines\nactually\n work \nwhich they \n seem to do:)\n",
//    10);

//return;

int offsetToChatrooms = 0;

Console.InputEncoding = Encoding.UTF8;
Console.OutputEncoding = Encoding.UTF8;

Console.Title = "CMPT Client";


var consoleLock = new object();

List<ChatRoom> chatRoomList = new List<ChatRoom>()
{
    //new(new Participant(default, default, "Marlon")),
    //new(new Participant(default, default, "Sandra")),
};

Dictionary<Participant, ChatRoom> chatRooms = new Dictionary<Participant, ChatRoom>();



Regex ipPortRegex = new Regex("(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3}):(\\d{1,5})");

Regex octett  = new Regex("(\\d{1,3})");
Regex _ushort = new Regex("(\\d{1,5})");
Regex dot     = new Regex("\\.");
Regex colon   = new Regex(":");

Regex[] ipPortSplittedRegex = new[]
{
    octett, dot, octett, dot, octett, dot, octett, colon, _ushort
};

Console.Write("\rStarting ChatClient");
Thread.Sleep(300);
Console.Write(".");
Thread.Sleep(300);
Console.Write(".");
Thread.Sleep(300);
Console.Write(".\n");
Thread.Sleep(300);

Console.Clear();

Console.WriteLine("Which IP-Address would you like to use?");

var adresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(x=>x.AddressFamily==System.Net.Sockets.AddressFamily.InterNetwork).ToList();

for (int i = 0; i < adresses.Count; i++)
{
    Console.WriteLine($"({i}) {adresses[i]}");
}

int idx = 0;

while (true)
{
    idx = Console.ReadKey(true).KeyChar-'0';
    if (idx >= 0 && idx < adresses.Count)
        break;
}


int currentChatRoomIdx = 0;

bool inMenuLoop = false;

Connector connector = new Connector(adresses[idx]);


const string paddingStr = "          ";

#region Mock
//new Thread(() =>
//{
//    int index = 0;

//    while (true)
//    {
//        Thread.Sleep(5000);

//        lock (consoleLock)
//            chatRoomList.Add(new ChatClient_Console.ChatRoom(new Participant(default, default, "NameX")));

//        index++;

//        if (inMenuLoop)
//            DrawMenu();
//    }
//}).Start();

//new Thread(() =>
//{
//    Random random = new Random();

//    int index = 0;

//    while (true)
//    {
//        Thread.Sleep(1000);

//        if (chatRoomList.Count > 0)
//        {
//            int room = random.Next(chatRoomList.Count - 1);

//            chatRoomList[room].RecieveMessage("Hello,\nthis TestMessage numer " + index, consoleLock);
//        }

//        index++;

//        if (inMenuLoop)
//            DrawMenu();
//    }
//}).Start();

//new Thread(() =>
//{
//    Random random = new Random();

//    while (true)
//    {
//        Thread.Sleep(7000);

//        if (chatRoomList.Count > 0)
//        {
//            int index = random.Next(chatRoomList.Count - 1);

//            lock (consoleLock)
//            {
//                lock(chatRoomList)
//                    chatRoomList.RemoveAt(index);
//            }
//        }

//        if (inMenuLoop)
//            DrawMenu();
//    }
//}).Start();
#endregion

connector.ReceiveMessage += (s, e) =>
{
    chatRooms[e.Sender].RecieveMessage(e.Message, consoleLock);
    Debug.WriteLine($"Message recieved from {e.Sender}: {e.Message}");
};

connector.NeueNetzteilnehmer += (list) =>
{
    lock (chatRoomList)
    {
        foreach (var item in list)
        {
            if (!chatRoomList.Any(x => x.Participant == item))
                Debug.WriteLine($"New participant: {item.Name}");
        }
    }

    lock (consoleLock)
    {
        ChatRoom? currentChatRoom = null;

        lock (chatRoomList)
        {
            if (chatRoomList.Count > 0)
            {
                currentChatRoom = chatRoomList[currentChatRoomIdx];

                chatRoomList.Clear();
            }


            foreach (var participant in list)
            {
                var chatRoom = chatRooms.GetOrCreate(participant, () => new ChatRoom(participant));

                chatRoomList.Add(chatRoom);
            }


            if (currentChatRoom != null)
            {
                currentChatRoomIdx = chatRoomList.IndexOf(currentChatRoom);

                if (currentChatRoomIdx == -1)
                    currentChatRoomIdx = 0;
            }
        }
    }

    if (inMenuLoop)
        DrawMenu();
};


Console.Clear();

Console.Write("Enter your name: ");

Console.CursorVisible = true;
connector.Name = Console.ReadLine()!;
Console.CursorVisible = false;

void SetWindowTitle() => Console.Title = $"CMPT Client - {connector.Name}         Listening on {connector.Address.ToString().Replace(".","_")} : {connector.Port}";


SetWindowTitle();






const string quickConnect_fileName = "QuickConnect.txt";

var quickConnect = new Dictionary<string, (IpAddress addr, ushort port)>();

var myQuickConnectEntryString = $"{connector.Name}, {connector.Address}:{connector.Port}";

var quickConnectEntryRegex = new Regex("(.*), (\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3}):(\\d{1,5})");

void UpdateQuickConnect(string[] lines)
{


    for (int i = 0; i < lines.Length; i++)
    {
        var match = quickConnectEntryRegex!.Match(lines[i]);

        if (!match.Success)
        {
            Debugger.Break();
            continue;
        }

        quickConnect![match.Groups[1].Value] = (new IpAddress(
            byte.Parse(match.Groups[2].Value),
            byte.Parse(match.Groups[3].Value),
            byte.Parse(match.Groups[4].Value),
            byte.Parse(match.Groups[5].Value)
            ),
            ushort.Parse(match.Groups[6].Value)
            );
    }
}

void UpdateQuickConnectFromFile()
{
    EnsureQuickConnectFileExists();
    var lines = File.ReadAllLines(quickConnect_fileName, Encoding.UTF8);
    UpdateQuickConnect(lines);
}

void EnsureQuickConnectFileExists()
{
    if (!File.Exists(quickConnect_fileName))
        File.Create(quickConnect_fileName).Close();
}

{
    EnsureQuickConnectFileExists();
    var lines = File.ReadAllLines(quickConnect_fileName, Encoding.UTF8);

    UpdateQuickConnect(lines);

    Array.Resize(ref lines, lines.Length + 1);

    lines[^1] = myQuickConnectEntryString;

    EnsureQuickConnectFileExists();
    File.WriteAllLines(quickConnect_fileName, lines);
}










try
{
    DrawMenu();

    while (true)
    {
        inMenuLoop = true;
        

        int dialogTop = offsetToChatrooms + chatRoomList.Count + 2;

        var info = Console.ReadKey(true);
        Console.CursorVisible = false;

        if (info.Key == ConsoleKey.Enter && currentChatRoomIdx<chatRoomList.Count)
        {
            inMenuLoop = false;
            chatRoomList[currentChatRoomIdx].ConsoleLoop(consoleLock, connector);
            Console.Clear();
            DrawMenu();
        }
        else if (info.Key == ConsoleKey.UpArrow && currentChatRoomIdx > 0)
        {
            lock (consoleLock)
            {
                Console.SetCursorPosition(0, offsetToChatrooms + currentChatRoomIdx);
                Console.ForegroundColor = ConsoleColor.DarkGray;
                WriteChatroomMenuEntry(chatRoomList[currentChatRoomIdx].Name);
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.ForegroundColor = ConsoleColor.White;
                WriteChatroomMenuEntry(chatRoomList[currentChatRoomIdx - 1].Name);
                Console.ForegroundColor = ConsoleColor.White;

                currentChatRoomIdx--;
            }
        }
        else if (info.Key == ConsoleKey.DownArrow && currentChatRoomIdx < chatRoomList.Count - 1)
        {
            lock (consoleLock)
            {
                Console.SetCursorPosition(0, offsetToChatrooms + currentChatRoomIdx);
                Console.ForegroundColor = ConsoleColor.DarkGray;
                WriteChatroomMenuEntry(chatRoomList[currentChatRoomIdx].Name);
                Console.SetCursorPosition(0, Console.CursorTop + 1);
                Console.ForegroundColor = ConsoleColor.White;
                WriteChatroomMenuEntry(chatRoomList[currentChatRoomIdx + 1].Name);
                Console.ForegroundColor = ConsoleColor.White;

                currentChatRoomIdx++;
            }
        }
        else if (info.Key == ConsoleKey.C)
        {
            inMenuLoop = false;

            Console.SetCursorPosition(0, dialogTop + 1);


            lock (consoleLock)
            {
                bool valid = false;

                string? errorMessage = "";

                string input = "";
                Console.CursorVisible = true;
                while (true)
                {
                    Console.Write($"           Enter IP:Port of the client you want to connect to: {input}");

                    bool isCancel = false;

                    

                    while (true)
                    {
                        info = Console.ReadKey(true);


                        UpdateQuickConnectFromFile();

                        if (info.Key == ConsoleKey.Enter)
                            break;
                        else if(info.Key == ConsoleKey.Escape)
                        {
                            isCancel = true;
                            break;
                        }
                        else if(info.Key == ConsoleKey.Backspace)
                        {
                            if(input.Length > 0)
                            {
                                input = input[0..^1];
                                Console.Write("\b \b");
                            }
                        }
                        else if(info.KeyChar != char.MinValue)
                        {
                            
                            string newInput = input + info.KeyChar;

                            bool isValidName = quickConnect.Keys.Any(x => x.StartsWith(newInput));

                            int index = 0;

                            if (!isValidName)
                            {
                                for (int i = 0; i < ipPortSplittedRegex.Length; i++)
                                {
                                    var m = ipPortSplittedRegex[i].Match(newInput, index);

                                    index += m.Length;

                                    if (!m.Success)
                                        break;
                                }
                            }

                            //partial match on full string: accepted
                            if (isValidName || index == newInput.Length)
                            {
                                input = newInput;

                                Console.Write(info.KeyChar);
                            }
                        }
                    }

                    if (isCancel)
                    {
                        Console.SetCursorPosition(0, dialogTop);
                        Console.Write(new string(' ', Console.BufferWidth * 10)); //just to be sure

                        break;
                    }


                    Match match = ipPortRegex.Match(input);

                    ushort port = 0;

                    byte octett1 = 0;
                    byte octett2 = 0;
                    byte octett3 = 0;
                    byte octett4 = 0;

                    valid = match.Success &&
                        byte.TryParse(match.Groups[1].Value, out octett1) &&
                        byte.TryParse(match.Groups[2].Value, out octett2) &&
                        byte.TryParse(match.Groups[3].Value, out octett3) &&
                        byte.TryParse(match.Groups[4].Value, out octett4) &&
                        ushort.TryParse(match.Groups[5].Value, out port);

                    var ip = new CmtpLib.Connection.IpAddress(
                            octett1,
                            octett2,
                            octett3,
                            octett4
                            );

                    if(!valid && quickConnect.TryGetValue(input, out var entry))
                    {
                        ip = entry.addr;
                        port = entry.port;
                        valid = true;
                    }

                    if (valid)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("\n           Trying to connect...");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    void HandleAlert(string msg) => errorMessage = msg;
                    connector.Alert += HandleAlert;
                    if (valid &&
                        connector.TryConnect(port, ip).GetAwaiter().GetResult())
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("\n           Successfully Connected!");
                        Console.ForegroundColor = ConsoleColor.White;
                        Thread.Sleep(500);
                        break;
                    }

                    connector.Alert -= HandleAlert;
                    if (!valid)
                        errorMessage = "Incorrect Format";

                    //lock(chatRoomList)
                    //    chatRoomList.Add(new ChatRoom(participant, participant.Name));


                    var clearLines = (Console.CursorTop - dialogTop) +1;

                    Console.SetCursorPosition(0, dialogTop);
                    Console.Write(new string(' ', Console.BufferWidth*clearLines));
                    Console.SetCursorPosition(0, dialogTop);

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("          " +  errorMessage);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.CursorVisible = false;
            }

            Console.Clear();
            DrawMenu();
        }
        else if(info.Key == ConsoleKey.N)
        {

            Console.CursorVisible = true;
            Console.SetCursorPosition(0, dialogTop + 1);
            Console.Write($"           Enter a new name: ");
            connector.Name = Console.ReadLine()!;
            Console.CursorVisible = false;

            Console.SetCursorPosition(0, dialogTop + 1);
            Console.Write(new string(' ', Console.BufferWidth));

            SetWindowTitle();
        }
    }
}
finally
{
    EnsureQuickConnectFileExists();
    var lines = File.ReadAllLines(quickConnect_fileName);

    EnsureQuickConnectFileExists();
    File.WriteAllLines(quickConnect_fileName, lines.Where(x => x != myQuickConnectEntryString));
}


















void WriteChatroomMenuEntry(string name, bool newLine = false)
{
    Console.Write($"{paddingStr}-{name}{(newLine?Console.Out.NewLine:string.Empty)}");
}

void DrawMenu()
{
    lock (consoleLock)
    {
        Console.Clear();

        Console.Write($@"

           _____ __  __ _______ _____         _____ _ _            _   
          / ____|  \/  |__   __|  __ \       / ____| (_)          | |  
         | |    | \  / |  | |  | |__) |_____| |    | |_  ___ _ __ | |_ 
         | |    | |\/| |  | |  |  ___/______| |    | | |/ _ \ '_ \| __|
         | |____| |  | |  | |  | |          | |____| | |  __/ | | | |_ 
          \_____|_|  |_|  |_|  |_|           \_____|_|_|\___|_| |_|\__|
                                                               
                                                               
                                                            
        Usage:
          Use Up and Down to choose a chatpartner
          Press Enter to enter the chatroom
          Press C to connect to a new ChatPartner
          Press N to rename yourself



        ChatRooms:

");

        if (chatRoomList.Count == 0)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine($"{paddingStr}No chatrooms");
            Console.ForegroundColor = ConsoleColor.White;
        }

        offsetToChatrooms = Console.CursorTop;

        for (int i = 0; i < chatRoomList.Count; i++)
        {
            ChatRoom? chatRoom = chatRoomList[i];

            if (i != currentChatRoomIdx)
                Console.ForegroundColor = ConsoleColor.DarkGray;

            WriteChatroomMenuEntry(chatRoom.Name, true);

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
