﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient_Console
{
    internal static class ChatUtils
    {
        public static int PrettyPrintMessage(string message, int offsetX, int MAX_LINE_WIDTH)
        {
            string messagePaddingStr = new string(' ', offsetX);

            string printStr = messagePaddingStr;

            int lines = 0;

            IEnumerable<string> words = message.Split(' ');

            words = words.SelectMany(word =>
            {
                var split = word.Split('\n');

                List<string> result = new List<string>();

                for (int i = 0; i < split.Length; i++)
                {
                    if (i == split.Length - 1)
                        result.Add(split[i]);
                    else
                        result.Add(split[i] + '\n');
                }

                return result;
            });

            var lineLength = 0;

            Stack<string> stack = new Stack<string>(words.Reverse());

            while (stack.TryPop(out string? word))
            {
                bool endsWithNewline = word.EndsWith("\n");

                if (endsWithNewline)
                    word = word[..^1];


                if (lineLength + word.Length + 1 > MAX_LINE_WIDTH)
                {
                    lines++;
                    printStr += '\n' + messagePaddingStr;
                    lineLength = 0;

                    if (word.Length > MAX_LINE_WIDTH)
                    {
                        printStr += word[..MAX_LINE_WIDTH];
                        stack.Push(word[(MAX_LINE_WIDTH - 1)..]);
                        continue;
                    }
                }

                if (endsWithNewline)
                {
                    lines++;
                    printStr += word + '\n' + messagePaddingStr;
                    lineLength = 0;
                }
                else
                {
                    lineLength += word.Length + 1;
                    printStr += word + ' ';
                }
            }

            Console.WriteLine(printStr[..^1]);

            return lines + 1;
        }
    }
}
