﻿namespace CmtpLib.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Connection;

[TestClass]
public class EncodingTests
{
    [TestMethod]
    public void WriteUint_ShouldWriteMsbFirst()
    {
        var stream = new MemoryStream(4);
        var expectedOutput = new byte[] { 0, 0, 0, 1 };
        uint input = 1;

        stream.WriteUint(input);

        byte[] written = stream.ToArray();

        Assert.AreEqual(expectedOutput.Length, written.Length);
        for (int i = 0; i < expectedOutput.Length; i++)
        {
            Assert.AreEqual(expectedOutput[i], written[i]);
        }
    }

    [TestMethod]
    public void WriteUshort_ShouldWriteMsbFirst()
    {
        var stream = new MemoryStream(2);
        ushort input = 1138;
        var expectedOutput = new byte[] { 0x04, 0x72 };

        stream.WriteUShort(input);

        byte[] written = stream.ToArray();

        Assert.AreEqual(expectedOutput.Length, written.Length);
        for (int i = 0; i < expectedOutput.Length; i++)
        {
            Assert.AreEqual(expectedOutput[i], written[i]);
        }
    }

    [TestMethod]
    public void ReadUShort_ShouldReadMsbFirst()
    {
        var stream = new MemoryStream(new byte[] { 0x04, 0x72 });

        var port = stream.ReadUShort();

        Assert.AreEqual((ushort)1138, port);
    }

    [TestMethod]
    public void ReadUInt_ShouldReadIpAddressCorrectly()
    {
        var stream = new MemoryStream(new byte[] { 127, 0, 0, 1 });
        var expectedString = "127.0.0.1";

        IpAddress ip = stream.ReadUInt();
        string actualString = ip.ToString();

        Assert.AreEqual(expectedString, actualString);
    }

    [TestMethod]
    public void ReadIpAddress_AfterWriting_ShouldReadCorrectly()
    {
        var stream = new MemoryStream(4);
        var input = new IpAddress(127, 0, 0, 1);
        
        stream.WriteAddress(input);
        stream.Position = 0;
        var output = stream.ReadAddress();

        Assert.AreEqual(input.AsDottedDecimal, output.AsDottedDecimal);
    }
}