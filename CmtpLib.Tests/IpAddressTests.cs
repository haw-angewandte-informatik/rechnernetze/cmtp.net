namespace CmtpLib.Tests;

using System.Net;
using Connection;

[TestClass]
public class IpAddressTests
{
    [TestMethod]
    public void ToUint_ShouldReturnCorrectNumber()
    {
        IpAddress adr = new(0, 0, 0, 1);

        uint ui = adr;

        Assert.AreEqual(1, (int)ui);
    }

    [TestMethod]
    public void FromUint_ShouldReturnCorrectIpAddress()
    {
        uint ui = 1;

        IpAddress adr = ui;
        
        Assert.AreEqual(1, (int)adr.Octett4);
    }

    [TestMethod]
    public void FromSystemIpAddress_ShouldReturnCorrectIpAddress()
    {
        IPAddress ip = IPAddress.Parse("0.0.0.1");

        IpAddress adr = ip;

        Assert.AreEqual(1, adr.Octett4);
        Assert.AreEqual("0.0.0.1", adr.AsDottedDecimal);
    }

    [TestMethod]
    public void AsDottedDecimal_ShouldReturnCorrectString()
    {
        IpAddress adr = new(0, 0, 0, 1);

        string s = adr.AsDottedDecimal;

        Assert.AreEqual("0.0.0.1", s);
    }

    [TestMethod]
    public void AsBigEndian_ShouldReturnCorrectOrder()
    {
        IpAddress adr = new(127, 0, 0, 1);
        byte[] expected = { 127, 0x00, 0x00, 0x01 };

        byte[] actual = adr.AsBigEndian;


        Assert.AreEqual(expected.Length, actual.Length);
        for (int i = 0; i < expected.Length; i++)
        {
            Assert.AreEqual(expected[i], actual[i]);
        }
    }

    [TestMethod]
    public void FromBigEndian_ShouldConstructCorrectAddress()
    {
        byte[] input = { 127, 0x00, 0x00, 0x01 };
        IpAddress expected = new(127, 0, 0, 1);
        Assert.AreEqual("127.0.0.1", expected.AsDottedDecimal);

        IpAddress actual = IpAddress.FromBigEndian(input);

        Assert.AreEqual(expected, actual);
    }
}